﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SocketMobile.Capture;
using Serilog;
using static SocketMobile.Capture.CaptureHelperDevice;

namespace capturesingleentry_uwp
{
    static class TestProperties
    {
        public static async Task<long> TestSetProperty(CaptureHelper capture)
        {
            List<CaptureHelperDevice> devices = capture.GetDevicesList();

            // !!! find first device of type 327702 kSktCaptureDeviceTypeScannerS550
            CaptureHelperDevice scanner = devices.FirstOrDefault<CaptureHelperDevice>();
            if (scanner == null)
            {
                Log.Debug($"TestSetPropert  - no scanner found");
                return -1;
            }

            // DeviceInfo
            ICaptureDeviceInfo info = scanner.GetDeviceInfo();
            Log.Debug($"Scanner DeviceInfo - name: {info.Name} guid: {info.Guid} type: {info.Type}");

            // GetFriendlyName
            FriendlyNameResult result = await scanner.GetFriendlyNameAsync();
            Log.Debug($"GetFriendlyNameAsync  - got scanner name: {result.FriendlyName}");

            // BT address
            

            // Battery level

           //
            return 0;
        }

        public static async Task<long> TestGetProperty()
        {

            return 0;
        }
    }
}
